
package net.assintates.Data.Model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Title implements Serializable
{

    @SerializedName("japanese")
    @Expose
    private String japanese;
    @SerializedName("pretty")
    @Expose
    private String pretty;
    @SerializedName("english")
    @Expose
    private String english;
    private final static long serialVersionUID = 1216155923316445873L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Title() {
    }

    /**
     * 
     * @param japanese
     * @param english
     * @param pretty
     */
    public Title(String japanese, String pretty, String english) {
        super();
        this.japanese = japanese;
        this.pretty = pretty;
        this.english = english;
    }

    public String getJapanese() {
        return japanese;
    }

    public void setJapanese(String japanese) {
        this.japanese = japanese;
    }

    public String getPretty() {
        return pretty;
    }

    public void setPretty(String pretty) {
        this.pretty = pretty;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

}
