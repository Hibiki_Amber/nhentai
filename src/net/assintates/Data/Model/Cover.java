
package net.assintates.Data.Model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cover implements Serializable
{

    @SerializedName("h")
    @Expose
    private Integer h;
    @SerializedName("t")
    @Expose
    private String t;
    @SerializedName("w")
    @Expose
    private Integer w;
    private final static long serialVersionUID = 1278141146245486479L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Cover() {
    }

    /**
     * 
     * @param w
     * @param t
     * @param h
     */
    public Cover(Integer h, String t, Integer w) {
        super();
        this.h = h;
        this.t = t;
        this.w = w;
    }

    public Integer getH() {
        return h;
    }

    public void setH(Integer h) {
        this.h = h;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public Integer getW() {
        return w;
    }

    public void setW(Integer w) {
        this.w = w;
    }

}
