
package net.assintates.Data.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import net.assintates.Data.Model.Page;

public class Images implements Serializable
{

    @SerializedName("cover")
    @Expose
    private Cover cover;
    @SerializedName("pages")
    @Expose
    private List<Page> pages = new ArrayList<Page>();
    @SerializedName("thumbnail")
    @Expose
    private Thumbnail thumbnail;
    private final static long serialVersionUID = 1935142786430627334L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Images() {
    }

    /**
     * 
     * @param cover
     * @param thumbnail
     * @param pages
     */
    public Images(Cover cover, List<Page> pages, Thumbnail thumbnail) {
        super();
        this.cover = cover;
        this.pages = pages;
        this.thumbnail = thumbnail;
    }

    public Cover getCover() {
        return cover;
    }

    public void setCover(Cover cover) {
        this.cover = cover;
    }

    public List<Page> getPages() {
        return pages;
    }

    public void setPages(List<Page> pages) {
        this.pages = pages;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

}
